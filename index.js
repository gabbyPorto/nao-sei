class Matricula {
  constructor(nome, sobrenome, nomeResponsavel, cpf, dataNascimento, telefoneResp, quemReside, residencia, idade){
    this.nome = nome;
    this.sobrenome = sobrenome;
    this.nomeResponsavel = nomeResponsavel;
    this.cpf = cpf;
    this.dataNascimento = dataNascimento; 
    this.telefoneResp = telefoneResp;
    this.quemReside = quemReside;
    this.residencia = residencia;
    this.idade = idade; 
    this.serie = prompt("Informe sua última série de conclusão").toLowerCase();
  }
}
let alunoUm = new Matricula("João Pedro", "Oliveira", "João Paulo", "20022020", "02/11/2003", "89028922", "Zé gotinha", "Rua do pea", "24");
console.log(alunoUm)
function Turma () {
  return function (serie) {
    if (serie === "infantil 1") {
      console.log("O discente está matriculado no infantil 2");
    } else if (serie === "1° do fundamental" || serie === "2° do fundamental" || serie === "3° do fundamental" || serie === "4° do fundamental") {
      console.log("O discente está matriculado no fundamental 1");
    } else {
      console.log("O discente está matriculado no fundamental 2");
    }
  }
}

let turmaDoAluno = Turma();
turmaDoAluno(alunoUm.serie); // Passa a série em letras minúsculas como argumento da função

//Discentes: Gabriely Porto e Natália de Sales. 